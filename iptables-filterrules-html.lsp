<% local data, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% htmlviewfunctions.displaycommandresults({"editchain", "deletechain", "createrule", "deleterule", "editrule", "createchain"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
	<table>
	<% local tab = data.value.table %>
	<% for j,chain in ipairs(data.value) do %>
		<tr><td>
		<% if chain.policy then %>
			<a href="<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller.."/editchain?chain="..chain.name.."&table="..tab.."&redir="..page_info.orig_action) %>"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/document-properties.png' width='16' height='16' title="Edit Chain"></a>
		<% else %>
			<a href="<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller.."/deletechain?submit=true&chain="..chain.name.."&table="..tab.."&redir="..page_info.orig_action) %>"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-remove.png' width='16' height='16' title="Delete Chain"></a>
		<% end %>
		<%= html.html_escape(chain.name) %>
		<% if chain.policy then io.write(" ("..html.html_escape(chain.policy)..")\n") end %>
		<% if chain.references then io.write(" ("..html.html_escape(chain.references).." references)\n") end %>
		</td></tr>
		<% for j,line in ipairs(chain) do %>
			<table>
			<tr><td width='80px' style='padding-left:40px'>
			<a href="<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller.."/createrule?table="..tab.."&chain="..chain.name.."&position="..j.."&redir="..page_info.orig_action) %>"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-add.png' width='16' height='16' title="Insert Rule"></a>
			<a href="<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller.."/deleterule?submit=true&table="..tab.."&chain="..chain.name.."&position="..j.."&redir="..page_info.orig_action) %>"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-remove.png' width='16' height='16' title="Delete Rule"></a>
			<a href="<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller.."/editrule?table="..tab.."&chain="..chain.name.."&position="..j.."&redir="..page_info.orig_action) %>"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/document-properties.png' width='16' height='16' title="Edit Rule"></a>
			</td>
			<td width='50px'><%= html.html_escape(line.packets) %></td><td width='50px'><%= html.html_escape(line.bytes) %></td>
				<td><%= html.html_escape(line.rule) %></td>
			</tr>
			</table>
		<% end %>
		<table>
		<tr><td width='80px' style='padding-left:40px'>
		<a href="<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller.."/createrule?table="..tab.."&chain="..chain.name.."&redir="..page_info.orig_action) %>"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-add.png' width='16' height='16' title="Append Rule"></a>
		</td></tr>
		</table>
	<% end %>
	<tr><td><a href="<%= html.html_escape(page_info.script..page_info.prefix..page_info.controller.."/createchain?table="..tab.."&redir="..page_info.orig_action) %>"><img src='<%= html.html_escape(page_info.wwwprefix..page_info.staticdir) %>/tango/16x16/actions/list-add.png' width='16' height='16' title="Create Chain"></a></td></tr>
	</table>
<% htmlviewfunctions.displaysectionend(header_level) %>
