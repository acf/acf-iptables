<% local data, viewlibrary, page_info, session = ...
html = require("acf.html")
%>

<% viewlibrary.dispatch_component("status") %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<% local header_level2 = htmlviewfunctions.incrementheader(header_level) %>
<% for i,tab in ipairs({"filter", "nat", "mangle"}) do %>
	<% htmlviewfunctions.displaysectionstart(cfe({label=tab}), page_info, header_level2) %>
	<table>
	<tr><td><%= html.html_escape(data.value[tab].chains) %> Chains</td></tr>
	<tr><td><%= html.html_escape(data.value[tab].rules) %> Rules</td></tr>
	</table>
	<% htmlviewfunctions.displaysectionend(header_level2) %>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
