<% local form, viewlibrary, page_info = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<%
local header_level = htmlviewfunctions.displaysectionstart(form, page_info)
local header_level2 = htmlviewfunctions.incrementheader(header_level)
for name,val in pairs(form.value) do val.name = name end
form.value.table.readonly = true
form.value.table.type = "text"
form.value.chain.readonly = true
form.value.position.readonly = true
htmlviewfunctions.displayformstart(form, page_info)

htmlviewfunctions.displaysectionstart(cfe({label="Rule Identification"}), page_info, header_level2)
htmlviewfunctions.displayformitem(form.value.table)
htmlviewfunctions.displayformitem(form.value.chain)
htmlviewfunctions.displayformitem(form.value.position)
htmlviewfunctions.displaysectionend(header_level2)

htmlviewfunctions.displaysectionstart(cfe({label="Basic Parameters"}), page_info, header_level2)
htmlviewfunctions.displayformitem(form.value.jump)
htmlviewfunctions.displayformitem(form.value.protocol)
htmlviewfunctions.displayformitem(form.value.in_interface)
htmlviewfunctions.displayformitem(form.value.out_interface)
htmlviewfunctions.displayformitem(form.value.source)
htmlviewfunctions.displayformitem(form.value.destination)
htmlviewfunctions.displayformitem(form.value["goto"])
htmlviewfunctions.displayformitem(form.value.fragment)
htmlviewfunctions.displayformitem(form.value.set_counters)
htmlviewfunctions.displaysectionend(header_level2)

htmlviewfunctions.displaysectionstart(cfe({label="Extended Parameters"}), page_info, header_level2)
htmlviewfunctions.displayformitem(form.value.comment)
htmlviewfunctions.displayformitem(form.value.addrtype_src_type)
htmlviewfunctions.displayformitem(form.value.addrtype_dst_type)
htmlviewfunctions.displayformitem(form.value.icmp_type)
htmlviewfunctions.displayformitem(form.value.src_range)
htmlviewfunctions.displayformitem(form.value.dst_range)
htmlviewfunctions.displayformitem(form.value.mac_source)
htmlviewfunctions.displayformitem(form.value.sports)
htmlviewfunctions.displayformitem(form.value.dports)
htmlviewfunctions.displayformitem(form.value.ports)
htmlviewfunctions.displayformitem(form.value.state)
htmlviewfunctions.displayformitem(form.value.tcp_sport)
htmlviewfunctions.displayformitem(form.value.tcp_dport)
htmlviewfunctions.displayformitem(form.value.udp_sport)
htmlviewfunctions.displayformitem(form.value.udp_dport)
htmlviewfunctions.displaysectionend(header_level2)

htmlviewfunctions.displayformend(form, header_level2)
htmlviewfunctions.displaysectionend(header_level)
%>
