local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.details(self)
	return self.model.getstatusdetails()
end

function mymodule.filterrules(self)
	return self.model.getrules("filter")
end

function mymodule.natrules(self)
	return self.model.getrules("nat")
end

function mymodule.manglerules(self)
	return self.model.getrules("mangle")
end

function mymodule.editchain(self)
	return self.handle_form(self, function() return self.model.read_chain(self.clientdata.table, self.clientdata.chain or "") end, self.model.update_chain, self.clientdata, "Save", "Edit Chain", "Chain saved")
end

function mymodule.createchain(self)
	return self.handle_form(self, function() return self.model.read_chain(self.clientdata.table) end, self.model.create_chain, self.clientdata, "Create", "Create New Chain", "Chain created")
end

function mymodule.deletechain(self)
	return self.handle_form(self, self.model.get_delete_chain, self.model.delete_chain, self.clientdata, "Delete", "Delete Chain", "Chain deleted")
end

function mymodule.editrule(self)
	return self.handle_form(self, function() return self.model.read_rule(self.clientdata.table, self.clientdata.chain or "", self.clientdata.position or "") end, self.model.update_rule, self.clientdata, "Save", "Edit Rule", "Rule saved")
end

function mymodule.createrule(self)
	return self.handle_form(self, function() return self.model.read_rule(self.clientdata.table, self.clientdata.chain, self.clientdata.position) end, self.model.create_rule, self.clientdata, "Create", "Create New Rule", "Rule created")
end

function mymodule.deleterule(self)
	return self.handle_form(self, self.model.get_delete_rule, self.model.delete_rule, self.clientdata, "Delete", "Delete Rule", "Rule deleted")
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.readrulesfile, self.model.updaterulesfile, self.clientdata, "Save", "Edit Rules File", "Rules File Saved")
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

return mymodule
